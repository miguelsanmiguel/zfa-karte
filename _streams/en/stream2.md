---
layout: stream
title:  Futur II
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---

## Welcome in Futur II
We will stream a handful of panels on this channel. The full Futur II-programme is listed below the video. Big thanks to Freundeskreis Videoclips, for letting us stream via their Youtube channel.
### Next event
**29.08., 15:00-16:30:** "Social Protest Movements – milestones on the way to global justice" with Dr. Juhi Tyagi, Dr. Esther Muinjangue, Limbert Sánchez Choque, Nathalie Nad-Abonji, Ferhad Ahma. The panel languages are German and Spanish. For translation click the link for your language of choice (D/ES) and open it in a new tab, please.


<iframe class="board16-9" src="https://www.youtube.com/embed/UBI9ZT9-xOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


If you need translation, click on the language button below and open it in a new tab.

{% include button.md text="Deutsches Audio" url="http://stream.foodada.de:8000/DE2" %}
{% include button.md text="English audio" url="http://stream.foodada.de:8000/EN2" %}
{% include button.md text="Audio español" url="http://stream.foodada.de:8000/ES2" %}
{% include button.md text="Audio português" url="http://stream.foodada.de:8000/PT2" %}


## Programm

<!--### Wednesday, 26.08.

---

***Time:*** 15:00-16:30
Event: Pannel - Digitality between Emancipation and Domination

***Speakers:*** Andrea Vetter, Kris de Decker, Sabine Nuss, Natalia Messer

***Description:***
The digital technology of the future is being developed today – by whom, and with what goals? From dreams of a high-tech digital planned economy to visions of a low-tech radical democratic society without expertise, many paths are possible. These visions are not free of contradictions: what form of digitality can be organized in a socially and ecologically just manner? Where are the dangers of domination, and what kind of emancipation can we expect from technology? We discuss different approaches and take up historical examples such as the Cybersin project in Chile


### Thursday, 27.08.

---

***Time:*** 15:00–16:30
Event: Institutions of Freedom and Solidarity? - Explorations beyond Market and State

***Speakers:*** Simon Sutterluetti, Stefan Meretz, Racquel Gutierrez, Eva von Redecker

***Translation:*** DE

***Description:***
Capitalism and state socialism have proven to be simply variations of destruction of nature, patriarchy, racism and wage labor. Beyond those alternatives, questions arise concerning the coordination of society: What can take the coordinating place of state and market? How to produce and distribute goods and services in order to justly satisfy human needs? How do we take global decisions without structures of rule and domination? How to organize a good life beyond compulsory labor? This is a conversation between Critical Theory, Feminism, South American liberation movement and Commons.

---

***Time:*** 17:00-18:30

***Event:*** Panel - School of the Good Life

***Speakers:*** Margret Rasfeld, Samira Ghandour, Ângela Biz Antunes, Rudaba Badakhshi, Franziska Pritzke, Peter Schulz, Vanessa de Oliveira Andreotti

***Description:***
What will a school look like in 2048? How do we learn and how do people meet here? Who decides what? "School" in 2048 will be very different than today. One thing is clear: It will be oriented towards a good life for all and: It will support people in living a solidary mode of living. To get a feeling what this could mean exactly, we want to dare a look into the future and therefore look through different glasses. For this purpose, we have invited people from the following areas to speak about their school utopia and discuss how a transformation to it can succeed. -->


### Saturday, 29.08.

---

***Time:*** 15:00-16:30

***Event*** Social Protest Movements – milestones on the way to global justice

***Speakers:*** Dr. Juhi Tyagi, Dr. Esther Muinjangue, Limbert Sánchez Choque, Nathalie Nad-Abonji, Ferhad Ahma

***Translation:*** DE/ES

***Description:***
Protest movements mushroom all over the globe. People come together in various locations to fight for a just and solidary future. The panel discussion will develop a global perspective on these protest movements. We want to investigate how different movements can learn and profit from each other on a global scale. And even further, how these movements will help to achieve a future in 2048 in which the global injustices are finally levelled out.
