{% assign top_list = site.spaces | where: 'in_sorted_list', false | reverse %}
{% for space in top_list %}
  - ## [{{ space.title }}]({{ space.url | prepend: site.baseurl }}){: .space-link}
{% endfor %}
{% assign spaces_list = site.spaces | where: 'in_sorted_list', true | sort: 'title' %}
{% for space in spaces_list | %}
  - ## [{{ space.title }}]({{ space.url | prepend: site.baseurl }}){: .space-link}
{% endfor %}

<!-- {% for space in site.spaces %}
  - <span class="space-meta">{{ space.date | date: "%b %-d, %Y" }}</span>
    ## [{{ space.title }}]({{ space.url | prepend: site.baseurl }}){: .space-link}
{% endfor %} -->
