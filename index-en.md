---
lang: en
permalink: /
layout: default
---
**You can find your room overview in the menu in the upper left corner by clicking on the three white stripes.**

{% include map.md %}

# Congress map
{:.page-heading }

Alternatively, you can use click on this [List of Spaces](spaces_list).
