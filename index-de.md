---
lang: de
permalink: /
layout: default
---
**Eure Raumübersicht findet ihr im Menü, dass ihr in der linken oberen Ecke ansteuern könnt.**

{% include map.md %}

# Karte des Kongresses
{:.page-heading }

Alternativ zur Karte gibt es auch eine [**Listenübersicht**](spaces_list).
