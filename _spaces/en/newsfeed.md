---
layout: space
title:  Newsfeed
in_sorted_list: false
svg: ticker
---

<!-- <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p> -->

This is your morning cup of coffee to get a good start for each day and stay informed throughout the whole congress. We will share relevant technical updates and give the hottest tips to the daily program.

---

*01.09., 14:00* **++ THANK YOU EVERYONE FOR A WONDERFUL CONGRESS ++**

The congress is over, the future is coming! For feedback on the congress, [**please mail us**](mailto:zfa-feedback@riseup.net). If you want to recap one of the panels or get photo impressions from the congress, check out all the videos [**on this site**](https://zukunftfueralle.jetzt/kongress/bilder-und-videos/).

**And if you have not yet paid your participation fee or in case you would like to donate for the congress, see all details [on this site](https://zukunftfueralle.jetzt/en/about-us/donations/).**


---

*29.08., 16:00* **++ Last workshop and podium day ++ Theater "The Trap" at the end ++**

The last full day has been running for quite some time now and we notice: technology is under control by now! :)

While podiums on the financial system, art and culture (incl. performance) and social movements are in progress, the rest of the day will focus on the transformative power of art. First the continuation of the art and culture podium will start at 17:00. At 20:00 the theater "The Trap" will be performed and streamed live. Riadh Ben Ammar, author of the play, says

> We should never forget, back in the 60's 70's and 80's, before the EU existed, there was always the possibility to travel between Africa and Europe. There was peace on the Mediterranean at that time. I was born in Tunisia and I remember at the end of the 80's, when I was little, I got my first little bike from my dad, who had been in Palermo in Italy. At that time many families did the big pre-wedding shopping in Palermo. There was a lot of going there and back. And we must not forget that there was no EU, but a lot of freedom of movement. And since the EU we need a visa. Since then the young people started to leave the country illegally and they never came back.
>
>What happens to these young people ? I'll show you today at 8 pm in the play "The Trap" on Zfa channel Futur 1

We wish you a nice day and look forward to seeing you at the joint closing ceremony tomorrow. As a reminder: We are still looking for people who would like to share their congress experience briefly on Sunday morning: What was special for you? Which moment inspired you? What connections between the individual topics and a comprehensive vision have become clear to you? Either "live" or via a recorded video. Please contact us at [**zfa-programm@riseup.net**](mailto:zfa-programm@riseup.net).

---

*28.08., 16:45* **++ Connecting Utopias starts right now ++ Last evening podium is coming up ++ Your photo for the photo mosaic ++ Tech-Host? ++ Workshop cancellation(s) ++**

The second workshop part of the day comes to an end. At **5 o'clock** we'll continue. You are all warmly invited to the **Connecting Utopias** workshop today. You will find the two workshop rooms in the congress area. A little later, at 8 pm, the last evening podium of the congress will start: "And how do we get to utopia? - Ways of social-ecological transformation".

For the aftermovie of the congress we would like to create a big **photo mosaic**. The film team: "In our final video we want to show all participants of this congress. Therefore it would be great if you take a picture of yourself with your laptop camera on Friday and send it to us. The picture will then be put together into a big mosaic, which will hopefully consist of all the pictures of the over 1000 participants". Send us your pictures to [zfa-doku@riseup.net](mailto:zfa-doku@riseup.net).

Um - already been Tech-Host? No? [**Please text us**](mailto:zfa-digital@riseup.net) and **become a Tech-Host**! :)

In other things: "Due to illness in the speakers' team, parts 2 and 3 of the "Community Accountability & Transformative Justice" event can be cancelled on Saturday (29.08.) will unfortunately not take place. The first part of the event (295) in the morning, 11-12:30, will take place as scheduled. Your Program Coordination"

---

*Aug 28th, noon* ++ Pictures and videos of the congress online ++ Bar constructions finished ++

The morning kick-off is over - **thanks Derya and Andreas!** -  and workshops are running as well as the panel on reconsidering love! Like all other streaming events, this morning kick-off will be available online in a few days. Then all those who were unable to attend this morning can follow Derya's explanations on reproductive justice. **Photos and videos** on various aspects of the congress events [**can be found here**](https://zukunftfueralle.jetzt/kongress/bilder-und-videos/).

Before we wish a good day, a small note on the bar: if you want to go to the bar tonight during or after the evening podium on "How do we get to utopia?", there is a small technical refinement to consider: **open the bar room in a new tab**, outside the congress area. The video rooms of the bar do not work inside the congress area software. So, if the digital bouncer team messed up your plans yesterday and didn't let you into the bar, don't be discouraged and try again today.

Have a nice day!

---

*Aug 27th, 11:30 pm* **++ Connect utopias! Open exchange on Friday ++ Program changes ++**

An eventful day just ended. **Huge  thanks to the the countless speakers, participants and helpers at all levels**. Today we experienced that even at our congress not every technical breakdown can be solved immediately. This may lead to postponement of one or the other schedule. We are now working on technical solutions on the one hand. But on the other hand we kindly ask for your understanding that technical problems may occur at an event with that many different technical solutions and problems. All of us are learning continuously - be it about our utopias, the ways to get there or the organization of a digital congress.

Now some information on Friday, the 3rd workshop day. As usual we'll start at 09:30: don't miss the morning kick-off with Derya Binışık! Along the day, podiums on mobility, the world of relationships and social guarantees await us all. The congress day will come to an end with a discussion on **"And how do we get to Utopia? Pathways of social-ecological Transformation"** with Elisabeth Jeglitzka, Uwe Meinhardt, Ceren Türkmen, Nilufer Koç and Linda Schneider. Special ip for people who are in Leipzig right now: from 15:00-18:00 the **Utopia Machine** is running hot again in "heiter und wolkig" in Plagwitz - come by, welcome!

Furthermore we pass on this invitation to all participants: "**Connect Utopias** - it's rolling! **Join us** for cross-strand exchange and networking. Again on Friday from 5 o'clock in the room "Connecting Utopias".

In other news: **Workshop (241)** Kreiskultur, for tomorrow Friday, 28.08. was unfortunately **cancelled**. :(

Have a good night and a wonderful utopian Friday.

P.S.: already taken over a tech host shift? There's even a poem about tech-hosts (which reads better than it sounds verbally speaking):

in digitality I'm lost.

there comes a tech-host.

the person I need most. (Somewhere in the world wide web, August, 2020)


---

*Aug 27th, 6:15 pm* **++ Don't be a Tech-Horst - become a Tech-Host! ++ Share your experiences with us ++ Submit open space contributions ++**

We just learned that delays also happen in the digital sphere. But what we encounter so often in our non-digital everyday life cannot knock us off at the digital congress. So we are all the more pleased that all the afternoon podiums with speakers from many different countries and from very different backgrounds have now started.

To make sure that not only the (technically complex) podiums are in good hands, but also the (technically less complex) workshops are going well, we are still looking for **tech hosts**. This task requires a short introduction from the orga-team and the willingness to be present at a 90-minute workshop. Please feel free to contact us by [**e-mail**](zfa-openspace@riseup.net).

Completely **different story: the closing on Sunday**. We are still looking for people who share brieg congress experiences on Sunday morning: What was special for you? Which moment inspired you? What connections between the individual topics and a comprehensive vision have become clear to you? We are happy about "live" footage as well as recorded videos. You are welcome to contact us at [**zfa-programm@riseup.net](mailto:zfa-programm@riseup.net).

And finally: at 19:00 the Open Space editorial office closes for today. If you would like to announce an Open Space for tomorrow, please do so [**by e-mail**](mailto:zfa-openspace@riseup.net).

Now we wish you an inspiring and entertaining evening!

---

*Aug 27th, 9:15 am* **++ Tag 3 startet ++ Finanzierungslücke noch nicht geschlossen ++ Schon Tech-Host gewesen?++**

Good morning everyone!

Kate Čabanová (Konzeptwerk Neue Ökonomie) and Werner Rätz (attac) are about to kick off day 3 with us. As always, the stream will be shared on [**this site**](streams/stream1) and the talks will be translated in English.

Shifting this congress into the digital sphere was a huge effort. But after one and a half days we already feel like it was definitely worth it. However, the challenges of the last months created a 40,000€ gap in our budget. So, please, help us closing that gap. Further information [**right here**](https://zukunftfueralle.jetzt/en/about-us/donations/).

Being a Tech-Host - this is the cool thin of the congress. How you get there? Write an e-mail to [**zfa-digital@riseup.net**](mailto:zfa-digital@riseup.net). Just bring a little bit of preparation time and time for tech-hosting a 90 minute workshop and you would help us out big time. Thank you :)

{% include button.md text="Donation info" url="https://zukunftfueralle.jetzt/en/about-us/donations/" %}

---

*Aug 26th, 8:15 pm* **++ Open Spaces online ++ First full day over and we're happy :) ++**

The first full day of the Future For All congress is almost over. Thanks to everyone for making this possible! Not everything played out perfectly but pretty much all events took place. Hundreds of participants found their workshops and watched different panel discussions. And we assume, that the biggest problem of today - finding your workshop room in our digital space – will be of less relevance in the coming days since all of you get to know the congress site step by step.

After today's experiences we want to spotlight the different networking formats of the congress. Meetings in [**affinity groups**](bezugsgruppen), your own workshops in an [**open space**](open-spaces) or just a good hangout in the [**bar**](bar) are possible at this congress. By the way: the bar is free for all, no registration needed at all. And talking of the open space: tomorrow the first open space events will take place. You're welcom to join in!

And now: have a good time streaming the evening panel on an "Economy of the future"!

---

*Aug 26th, 2:15 pm* **++ "Waldweit unterwegs" postponed to August, 27th, 08:45 ++**

The outdoor Art for Utopia event "Waldweit unterwgs" will take place tomorrow (27.08.) at 08:45. The meeting point (bus stop Nonnenweg) remains the same. Everyone is welcome!

---

*Aug 26th, 12:30 pm* **++ Workshop registrations still possible – but sometimes tricky ++**

You have registered for a workshop [**here**](https://anmeldung.zukunftfueralle.jetzt/), but you don't find the workshop room in your personal [**congress site**](https://kongressgelaende.zukunftfueralle.jetzt/)? This might be *the* way to your room:
1. Go to the registration site: [**click here**](https://anmeldung.zukunftfueralle.jetzt/)
2. Scroll down until you see the respective workshop. Fount it? Go ahead, please:
3. Click "cancel"/"abmelden" on the right side. The website will reload.
4. Scroll down again until you see the respective workshop and click "register"/"anmelden"
5. A new link token will be created. You can see it on the very page and you will also receive an e-mail containing this linl. With this new link, the workshop room should be free for you.

---

*Aug 26th, 11:15 am* **++ First regular day kicks off ++ Check dial-ins ++**

After an inspirational kick-off with Natasha A. Kelly we are heading into the first regular congress day. This means: panels, workshops and networking all day :)

If you want to dial-in into your workshop via telephone, note that a couple of phone numbers have changed. [**See all numbers here.**](https://kongresskarte.zukunftfueralle.jetzt/en/dial-in). And if you are already on the [**"kongressgelände"**](https://kongressgelaende.zukunftfueralle.jetzt/) but do not find the workshop rooms, check this out: Click on the three bars (a.k.a. burger menu) in the top left corner and all the rooms you have access to will pop up. Enjoy!

Today we have five public livestreams awaiting you, panels on agriculture & food sovereignity, energy, digitality, housing and a great evening live panel discussion on "Econonmy of the Future". More information [**here**](livestreams) and on the respective stream sites.

---

*Aug 25th, 3:50 pm* **++ The congress kicks off! ++**

Let’s go! The congress kicks off with inspiring talks by Bini Adamczak, Nina Treu und Kai Kuhnhenn. Watch the whole event in our livestream venue [**Futur I**](streams/stream1). In case you missed registration for workshops: no worries. You can still [**register here**](https://contacts.knoe.org/content/registration-future-all). The only difference: you cannot apply for any workshop you want. 2/3 of workshop spots are taken already and from this evening onwars we will offer all free workshop seats [**right here**](https://anmeldung.zukunftfueralle.jetzt/).

We are looking forward to an inspirational congress and six days addressing the questions: how do we want to live in 2048? And how do we get there?

P.S.: Today's opening panel starts at eight. It will be livestreamed at your [**favourite livestream**](streams/stream1) and there will, of course, be translation.


---
*Aug 24th, evening* **++ One more night and then we'll kick off the congress ++**

After a very long day the "Zukunftszentrale" ("Future's head office") is set to get us all on the way tomorrow. The first congress will start with the press conference at 10am. At 4 pm we will welcome all of you via livestream, followed by a keynote from Bini Adamczak (4.30pm) and the opening panel (8pm). [**All streams will be available on channel Futur I.**](streams/stream1)

---

*Aug 20th, 10:30 am* **++ The congress map is online ++**<br>

Dear world, last night the congress map popped up on our website. Good news: it will stay there! We only need to add a few links and information, make everything available for the public and – voilà! – the congress can kick off.

Looking forward to seeing you soon.

---
