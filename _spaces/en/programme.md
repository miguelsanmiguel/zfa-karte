---
layout: space
title:  "Programme"
in_sorted_list: true
svg: programme
---
At the Future for All Congress you can discover over 200 workshops, 16 thematic panel discussions and the evening panels tackling the biggest burning questions concerning a Future for All. Furthermore, in Leipzig you can join our diverse offline Arts for Utopia programme.

The workshops are accessible for pre-registered participants only. [Please, click your registration link before you click on the button below or on the entrance button at the top of this website. This link opens the congress gates.]

{% include button.md text="Go directly to the workshop area" url="https://kongressgelaende.zukunftfueralle.jetzt/" %}

However, spontaneous guests have plenty to explore, too, such as the livestreams of all panel discussions. [**See here**](livestreams) for an overview of our public livestreams. There are also plenty of non-digital events happenning in Leipzig. [**Check out the offline events.**](offline-events)

Besides the diverse workshop-formats, the digital congress opens up new ways of exchange. In diverse spaces and cities in Germany you can find your local livestreaming events during the congress. For an overview of the events [**check this site**](livestreaming-events). Furhermore you can participate at different networking and self-organised discussion formats. Click on the links for more information on [**affinity groups**](bezugsgruppen), [**open space**](open-spaces) and [**"The Future"**](bar) the 24h-congress online bar.

{% include button.md text="Full Programme online" url="https://pretalx.com/zukunftfueralle/schedule/?" %}

{% include button.md text="Download programme as PDF" url="https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/zfa-programme-overview-2.pdf" %}
