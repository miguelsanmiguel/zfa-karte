---
layout: space
title:  "FLINT* space"
in_sorted_list: true
svg: flint
---

## Hello, welcome to the Future For All FLINT*-space!

Hello!
Welcome to the FLINT*-space of the congress! If you like, you can introduce yourself by name and pronouns. FLINT\* is the abbreviation for women, lesbian, inter\*, non-binary and trans - if this does not suit you, we ask you to leave.
We want to create a safe space where anger may have their place above the circumstances. In case of discrimination, we reserve the right to moderation.

{% include button.md text="To the FLINT\*-space" url="https://kongressgelaende.zukunftfueralle.jetzt/rooms/231bb22d-223b-48ef-8968-526b8ce0853d" %}