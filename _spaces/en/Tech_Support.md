---
layout: space
title:  "Technical support"
in_sorted_list: true
svg: support
---
**First of all:** If you have not yet clicked the registration link which you received via e-mail: please do so before clicking on link buttons to the various congress rooms on this website or before clicking on "Entrance" at the top of this page. If you have not clicked the link yet you will not be able to enter the congress software 'venueless'. If you have activated the "local storage" in your browser, your access token will be saved and you can use all buttons shown on this website to go directly to the linked room. Alternatively, you can keep up your privacy protection, keep the local storage locked and always enter venueless  via your personal access link. The buttons on this page will not work for you.

**Second:** If you are already on the [**"kongressgelände"**](https://kongressgelaende.zukunftfueralle.jetzt/) but do not find the workshop rooms, check this out: Click on the three bars (a.k.a. burger menu) in the top left corner and all the rooms you have access to will pop up. Enjoy!

And now:

**Hello and welcome!**

We are here to solve any technical problems that may arise during your stay at the congress as quickly as possible. Since each of us can only help one person at a time, our resources are limited. That's why we've uploaded a number of guides on this page, which may help you to improve your technical congress experiences by yourself:

* [Congress manual](https://zukunftfueralle.jetzt/en/congress/congress-manual/)
* [**Tech-Host Manual**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung_eng.pdf) PDF
* [**Big Blue Button user guide**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-button-for-participants-and-tips-for-pleasant-video-conferences.pdf) PDF
* [**Guidelines for participants**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Guidelines-for-Participants.pdf) PDF
* [**Online Manual for Presenters**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Online-Manual-for-Presenters_2.0.pdf) PDF

## Availability
If that doesn't help, feel free to contact us, **preferably via a support chat in venueless [the congress software]** or call us at +4934165673750. Due to capacity reasons, we can only process the individual problems of registered participants. Sorry.
