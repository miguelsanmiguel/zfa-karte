---
layout: space
title:  "Open space"
in_sorted_list: true
svg: open
---
You would like to dvelwe deeper into your topic? Or perhaps focus on something, which has not yet been covered by our programme adequately? Or you simply cannot wait to continue with an exciting debate that had just sparkled elswhere - this is your place to be! We offer daily Open Spaces from Wednesday to Saturday, 5 p.m. to 6:30 p.m.

If you would like to use the Open Space, please send us an email latest at 7:30 p.m. the previous day to [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) with all necessary information (see below). If we should receive more requests for an Open Space than we can fullfill on one day, we will meet a selection, so please suggest us an alternative day for your request. We behold the right to reject requests for spaces, which violate the main principles of the congress (based on our [**Compass for Utopias**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf), in German only :/).

Every morning around 9 am we will publish the Open Space programme for the day (including the corresponding rooms) at the Open Space Board at the bottom of this page.

The Open Space events will take place in the digital bar [**„The Future“**](bar). There you can simply enter the room of the event of your choice. An overview of the allocation of topics and rooms can be found in the pad on the site of the bar. Each room is for a maximum number of 15 people only; but there are rooms as a fallback option, if it gets too full.

{% include button.md text="Go to Bar/Open Spaces" url="bar" %}

Your request for an Open Space slot via the email [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) should contain following information:

* Title and summary of the topic offered
* Purpose of the offer (exchange/discussion regarding…; input/workshop on…; networking for…)
* Day you want it to take place (+ possibly alternatives)
* Language of the event
* Particular needs


**All Open Spaces on Saturday will be hold in German.** If you want to add English Open Spaces, just go to the [bar](bar) and announce your subject, the open space time and a bar room on the pinboard.

|Room and Link|Short information on workshop/event|Language(s)
| :------------- | :---------- | :-----------: |
|[**Open Space 1**](https://jitsi.technischerpunkt.org/Z-Kabine1){: target='_blank'}| **Gemeinsam für eine Zukunft für alle! Fallstricke rund um: "Interkulturalität" - "Othering & Kulturalisierung" - "Alltagsnaher Umgang mit Verschiedenheit".** Im Workshop "Kultursensible Vermittlung von Nachhaltigkeit" haben wir folgendes Spannungsfeld entdeckt: Einerseits gibt es reale Unterschiede zwischen Menschengruppen hinsichtlich ihrer Prägungen, Haltungen, Routinen, Kulturtechniken, Privilegien, Erfahrungen etc.; Um breite Allianzen für eine Zukunft für alle zu gestalten, brauchen wir alltagsnahe Umgänge mit diesen Verschiedenheiten. Gleichzeitig gibt es Fallstricke, Machtverhältnisse und (konstruierte und real existierende) Verschiedenheiten zu reproduzieren und zu verstetigen. Wie können wir mit Differenzen konstruktiv umgehen ohne ein Othering zu betreiben? Wie können wir uns vor einer möglichen Handlungsunfähigkeit schützen, die droht, wenn wir aus Angst vor einem Othering den Umgang mit Verschiedenheit vermeiden? Wie können wir utopisch Umgang mit diesen wohl immer wieder auftretenden Herausforderungen finden? Austausch und Diskussion sowie möglicherweise Vernetzung zu dem Thema mit einigen Teilnehmenden und den Moderator*innen des Workshops 196. Weitere Menschen sind herzlich eingeladen. Eingereicht von Christoph | Deutsch |
|[**Open Space 2**](https://jitsi.technischerpunkt.org/Z-Kabine2){: target='_blank'}|**Kombinierte Beratungsstellen**<br>...als Alternative zum heutigen Arbeitsamt um niedrigschwellig Selbstwirksamkeitserfahrungen, Potentialfindung und -entfaltung zu ermöglichen. Wir tauschen uns dazu aus, wie das aussehen und gelingen kann und haben Raum, uns zu Vernetzen. Eingereicht von Doris|Deutsch|
| [**Open Space 3**](https://jitsi.technischerpunkt.org/Z-Kabine3){: target='_blank'}|**Utopia ist machbar** 9-minütige Performance zur Einführung in das Thema Utopie, ca. 15 minütige Vorstellung eines Thesenpapieres dazu, wie eine Utopie konkreter aussehen kann (www.utopia-ist-machbar.de), danach Diskussion. Eingereicht von Lucki| Deutsch|
