---
layout: space
title:  "Workshops"
in_sorted_list: true
svg: workshops
---
<!-- {% include button.md text="Zugang Workshopbereich" url="https://kongressgelaende.zukunftfueralle.jetzt/" %}-->

Die Workshops sind ein zentraler Baustein uns weiterzubilden. In vierzehn Themensträngen unterteilt ([mehr Infos [**hier**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Strang_Informationen.pdf)), greifen wir spezifischen Fragen auf, die für eine gerechte und ökologische Zukunft für alle relevant sind. Darauf aufbauend kommen wir miteinander in Austausch, entwerfen gemeinsam Utopien und zeichnen die Wege, auf denen wir sie erreichen können.

Jeden Tag besteht die Möglichkeit, Eindrücke aus den Workshops in verschiedenen Räumen weiterzudenken. Mittags könnt ihr in euren [**Bezugsgruppen**](bezugsgruppen) zusammenkommen. Am späteren Nachmittag verknüpfen wir in [**"Utopien verbinden!"**](synergieraum) die Workshopinhalte zu umfassenden Utopien, während ihr in den [**Open Spaces**](open-spaces) eure eigenen Themen und Formate anbieten oder die spontanen Angebote anderer wahrnehmen könnt. Der [**BIPoC-**](bipoc-space) und der [**FLINT\*-Space**](flint-space) bieten Rückzugs- und Austauschmöglichkeiten zum Kongressgeschehen, aber auch zu noch weiterreichenden Ebenen. Und wenn das Tagesprogramm abgeschlossen ist, gibt es auch noch die [**Bar**](bar), in der ihr euch (unter anderem) über das Erlebte des Tages austauschen könnt.



{% include button.md text="Zugang Workshopbereich" url="https://kongressgelaende.zukunftfueralle.jetzt/" %}

Die Anmeldung für Workshops war regulär bis zum 20.08. um 18.00 möglich. Du kannst dich [**weiterhin für den Kongress anmelden**](https://contacts.knoe.org/content/registration-future-all), jedoch nicht für einzelne Workshops. Für die kurzfristige Vergabe der Workshops versuchen wir ein System aufzusetzen was es dir erlauben wird, dich mit einem Tag Vorlauf für noch nicht volle Workshops anzumelden

Für die kurzfristige Vergabe der Workshops startet am Dienstag Abend [**diese Webseite**](https://anmeldung.zukunftfueralle.jetzt/). Dort kannst du selber Plätze in Workshops buchen, die noch nicht vollbelegt sind. Derzeit sind die Workshops zu etwa 2/3 ausgelastet.


