---
layout: space
title:  "FLINT* space"
in_sorted_list: true
svg: flint
---

## Hallo! Willkommen im FLINT\*-Space des Kongresses!

Wenn du möchtest, kannst du dich mit Namen und Pronomen vorstellen. FLINT\* ist die Abkürzung für Frauen, Lesben, inter*, nichtbinär und trans
\- wenn das nicht für dich passt, bitten wir dich, zu gehen.
Wir wollen einen sicheren Raum schaffen, in dem gleichzeitig auch Wut über die Verhältnisse ihren Platz haben darf. Bei diskriminierenden Äußerungen behalten wir uns jedoch die Moderation vor.

{% include button.md text="Zum FLINT\*-Space" url="https://kongressgelaende.zukunftfueralle.jetzt/rooms/231bb22d-223b-48ef-8968-526b8ce0853d" %}