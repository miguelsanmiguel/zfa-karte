---
layout: space
title:  "Open Space"
in_sorted_list: true
svg: open
---
Ihr möchtet ein Thema vertiefen? Ihr wollt den Fokus auf eine Thematik legen, die eurer Ansicht nach im Programm noch nicht ausreichend beleuchtet wird? Oder ihr müsst eine anregende Diskussion fortführen, die sich an anderer Stelle entwickelt hat? Genau dafür ist der Open Space da. **Mittwoch bis Samstag bieten wir täglich von 17:00 bis 18:30 Uhr Open Spaces an.**

Wenn ihr einen Open Space gestalten möchtet, schickt uns bitte spätestens bis 19:30 am Vortag eine Mail an [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) mit allen notwendigen Informationen (siehe unten). Sollten an einem Tag mehr Open Space Vorschläge gemacht werden als verfügbar sind, treffen wir eine Auswahl. Wir bitten euch deshalb, mögliche Ausweichtage vorzuschlagen. Zudem behalten wir uns vor, Angebote abzulehnen, die der grundsätzlichen Ausrichtung des Kongresses entgegenstehen ([**zur Orientierung**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf)).

Jeden Morgen ab 09:00 Uhr veröffentlichen wir das Open Space Programm des Tages am Open Space Brett am Ende dieser Seite. Die Open Space-Veranstaltungen finden dann in der [**digitalen Bar „Die Zukunft“**](bar) statt. Dort könnt ihr einfach dem Raum beitreten, in dem die Veranstaltung eurer Wahl stattfindet. Eine Übersicht über die Zuteilung von Themen und Räumen findet ihr auch direkt im Pad auf der Seite der Bar (ganz unten auf der Barseite). Die Räume in der Bar sind je für max. 15 Personen, sollte es zu voll werden, könnt ihr noch Ausweich-Barräume nutzen.


{% include button.md text="Zu den Open Spaces" url="bar" %}

Deine Open Space Anmeldung bei [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) sollte Folgendes beinhalten:

* Titel und Kurzbeschreibung zum Thema des Angebots
* Anliegen des Angebots (Austausch/Diskussion zu…; Input/Workshop zu…; Vernetzung zu…)
* Tag an dem das Angebot stattfinden soll (+ evtl. Ausweichoption)
* Sprache des Angebots
* maximale Teilnehmer\*innen-Anzahl
* spezielle Bedarfe

## Open Space Programm am 27.08., 17:00-18:30

|Raum und Link|Veranstaltungsinfos|Sprache|
| :------------- | :---------- | :-----------: |
|[**Open Space 1**](https://jitsi.technischerpunkt.org/Z-Kabine1){: target='_blank'}| **Gemeinsam für eine Zukunft für alle! Fallstricke rund um: "Interkulturalität" - "Othering & Kulturalisierung" - "Alltagsnaher Umgang mit Verschiedenheit".** Im Workshop "Kultursensible Vermittlung von Nachhaltigkeit" haben wir folgendes Spannungsfeld entdeckt: Einerseits gibt es reale Unterschiede zwischen Menschengruppen hinsichtlich ihrer Prägungen, Haltungen, Routinen, Kulturtechniken, Privilegien, Erfahrungen etc.; Um breite Allianzen für eine Zukunft für alle zu gestalten, brauchen wir alltagsnahe Umgänge mit diesen Verschiedenheiten. Gleichzeitig gibt es Fallstricke, Machtverhältnisse und (konstruierte und real existierende) Verschiedenheiten zu reproduzieren und zu verstetigen. Wie können wir mit Differenzen konstruktiv umgehen ohne ein Othering zu betreiben? Wie können wir uns vor einer möglichen Handlungsunfähigkeit schützen, die droht, wenn wir aus Angst vor einem Othering den Umgang mit Verschiedenheit vermeiden? Wie können wir utopisch Umgang mit diesen wohl immer wieder auftretenden Herausforderungen finden? Austausch und Diskussion sowie möglicherweise Vernetzung zu dem Thema mit einigen Teilnehmenden und den Moderator*innen des Workshops 196. Weitere Menschen sind herzlich eingeladen. Eingereicht von Christoph | Deutsch |
|[**Open Space 2**](https://jitsi.technischerpunkt.org/Z-Kabine2){: target='_blank'}|**Kombinierte Beratungsstellen**<br>...als Alternative zum heutigen Arbeitsamt um niedrigschwellig Selbstwirksamkeitserfahrungen, Potentialfindung und -entfaltung zu ermöglichen. Wir tauschen uns dazu aus, wie das aussehen und gelingen kann und haben Raum, uns zu Vernetzen. Eingereicht von Doris|Deutsch|
| [**Open Space 3**](https://jitsi.technischerpunkt.org/Z-Kabine3){: target='_blank'}|**Utopia ist machbar** 9-minütige Performance zur Einführung in das Thema Utopie, ca. 15 minütige Vorstellung eines Thesenpapieres dazu, wie eine Utopie konkreter aussehen kann (www.utopia-ist-machbar.de), danach Diskussion. Eingereicht von Lucki| Deutsch|






