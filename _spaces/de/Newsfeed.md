---
layout: space
title:  "Kongressticker"
in_sorted_list: false
svg: ticker
---

<!-- <p class="rss-subscribe"><a href="{{ "/feed.xml" | prepend: site.baseurl }}">Via RSS</a> abbonieren</p> -->

Hier bleibt ihr während der Kongresstage auf dem Laufenden. Wir posten an dieser Stelle je nach Bedarf technische Updates und weisen täglich auf besondere Programmschmankerl hin. Schaut also gerne jeden Tag mal vorbei!

---

*01.09., 14:00* **++ VIELEN DANK FÜR EINEN WUNDERBAREN KONGRESS ++**

Der Kongress ist vorbei, die Zukunft liegt vor uns! Für Feedback zum Kongress, [**mailt uns gerne**](mailto:zfa-feedback@riseup.net). Eine Sammling von Bildern und Videos findet ihr [**hier**](https://zukunftfueralle.jetzt/kongress/bilder-und-videos/).

**Und falls ihr eure Teilnahmegebühr noch nicht bezahlt habt oder uns noch mit einer Spende unterstützen möchtet, findet ihr [**alle Infos hier++](https://zukunftfueralle.jetzt/ueber-uns/spenden/).**

---
*29.08., 16:00* **++ Letzter Workshop- und Podiumstag ++ Theater "Die Falle" zum Abschluss ++**

Der letzte volle Tag läuft schon seit geraumer Zeit und wir merken: die Technik haben wir inzwischen ziemlich gut im Griff! :)

Während gerade Podien zum Finanzsystem, Kunst und Kultur (incl. Performance) und sozialen Bewegungen laufen, legen wir in den letzten Stunden des Tages den Fokus auf die transformative Kraft der Kunst: zuerst flgt um 17:00 die Fortsetzung des Kunst- und Kultur-Podiums. Ab 20:00 wird das Theater "Die Falle" aufgeführt und livegestreamt. Riadh Ben Ammar, Autor des Stücks, sagt dazu:

> Wir sollten nie vergessen, damals in den 60ern 70ern und 80ern, bevor es die EU gab, gab es immer die Möglichkeiten zwischen Afrika und Europa zu reisen. Am Mittelmeer gab´s damals Frieden. Ich bin in Tunesien geboren und ich erinnere mich ans Ende der 80er, als ich klein war, habe ich mein erstes kleines Fahrrad von mein Papa bekommen, der in Palermo in Italien gewesen war. Damals haben auch viele Familien vor der Hochzeit den großen Einkauf in Palermo gemacht, es gab viel hin und zurück. Und das dürfen wir nicht vergessen, es gab keine EU, aber viel Bewegungsfreiheit. Und seit der EU brauchen wir ein Visum. Seit dem haben die jungen Leute angefangen, illegal das Land zu verlassen und sie kamen nicht mehr zurück.
>
>Was passiert mit diesen jungen Leuten? Das zeige ich euch heute um 20 Uhr bei dem Theaterstück „Die Falle“ über Zfa-Sender Futur 1

Wir wünschen euch einen schönen Tag und freuen uns, wenn ihr morgen beim gemeinsamen Abschluss dabei seid. Diesbezüglich zur Erinnerung: Wir suchen noch Menschen, die am Sonntag morgen Lust haben, ihre Kongresserfahrung kurz zu teilen: Was war für dich besonders? Welcher Moment hat dich inspiriert? Welche Verbindungen zwischen den einzelnen Themen und einer umfassenden Vision ist für dich deutlich geworden? Entweder "live" oder über ein eingespieltes Video. Melde dich gerne unter [**zfa-programm@riseup.net**](mailto:zfa-programm@riseup.net).

---

*28.08., 16:45* **++ Utopien verbinden geht gleich los ++ Letztes Abendpodium steht bevor ++ Euer Foto fürs Fotomosaik ++ Tech-Host?! ++ Workshopabsage(n) ++**

Der zweite Workshop-Teil des Tages neigt sich dem Ende zu. Um **17 Uhr** geht's dann schon weiter. Heute seid ihr alle herzlich zum **Utopien verbinden** eingeladen. Die beiden Räume dafür findet ihr im Kongressgelände. Wenig später startet dann auch schon das letzte Abendpodium des Kongresses "Und wie kommen wir zur Utopie? - Wege der sozial-ökologischen Transformation".

Für den Aftermovie des Kongresses möchten wir ein großes **Fotomosaik** erstellen. Dazu das Filmteam: "In unserem Abschlussvideo möchten wir zeigen wer alles an diesem Kongress teilnimmt. Dafür wäre es großartig wenn ihr mit eurer Laptop-Kamera am Freitag ein Bild von euch macht und es uns zuschickt. Das Bild wird dann zu einem großen Mosaik zusammengesetzt, das im besten Fall aus allen Bildern der über 1000 Teilnehmenden besteht." Schickt uns eure Bilder an [zfa-doku@riseup.net](mailto:zfa-doku@riseup.net).

Ähm – schon Tech-Host gewesen? Nein? [Bitte hier melden](mailto:zfa-digital@riseup.net) und **Tech-Host werden**! :)

In weiteren Dingen: "Wegen krankheitsbedingten Ausfalls im Referent\*innen-Team können am Samstag (29.08.) die Teile 2 und 3 der Veranstaltung "Community
Accountability & Transformative Justice" leider nicht stattfinden. Der erste Teil der Veranstaltung (295) am Vormittag, 11-12:30 Uhr, findet ungeändert statt. Eure Programm-Koordination"

---

*28.08., mittag* **++ Bilder und Videos vom Kongress verfügbar ++ Barreparaturen ++**

So, der Morgenauftakt ist gelaufen – Danke Derya und Andreas! Wie alle anderen streambaren Veranstaltungen wird auch dieser Morgenauftakt in wenigen Tagen online zur Verfügung gestellt. Dann können auch alle, die heute Morgen verhindert waren, Deryas Ausführungen zur **reproduktiven Gerechtigkeit** folgen. **Fotos und Videos** zu verschiedensten Aspekten des Kongressgeschehens findet ihr [**hier**](https://zukunftfueralle.jetzt/kongress/bilder-und-videos/).

Bevor wir uns mit den besten Wünschen für euren Tag verabschieden noch ein kleiner Hinweis: wenn ihr heute während oder nach dem **Abendpodiun zu "Wie kommen wir zur Utopie?"** in die Bar gehen möchtet, gibt es eine kleine technische Feinheit zu beachten: **öffnet den Barraum in einem neuen Tab**, außerhalb des Kongressgeländes. Die Videoräume der Bar funktionieren nämlich nicht innerhalb der Kongressgeländesoftware. Also, sollte euch das digitale Türsteher*innen-Team gestern einen Strich durch die Rechnung gemacht und euch nicht in die Bar eingelassen haben, seid nicht verzagt und probiert es heute nochmal.

Einen schönen Tag euch!

---

*27.08., 23:15* **++ Utopien verbinden! Am Freitag gemeinsam austauschen ++ Programmänderungen ++**

Ein ereignisreicher Tag ging gerade zuende. Wir **bedanken uns bei den unzähligen Referent\*innen, Teilnehmer\*innen und Helfer\*innen auf allen Ebenen**. Wir haben heute erfahren müssen, dass auch bei unserem Kongress nicht jedes technische Problem umgehend behoben werden und somit der ein oder andere Ablaufplan verschoben werden kann. Nun arbeiten wir einerseits an technischen Lösungen, sind aber andererseits auch auf Verständnis angewiesen, dass bei so vielen Veranstaltungen und so vielen verschiedenen technischen Lösungen und Problemen auch mal was anders läuft als geplant. Wir lernen alle ununterbrochen – ob es um unsere Utopien, die Wege dorthin oder um die Ausrichtung eines digitalne Kongresses geht.

Für Freitag, den 3. Workshoptag schonmal ein paar Infos. Geht wie gewohnt früh um 09:30 los: den Morgenauftakt mit Derya Binışık auf keinen Fall entgehen lassen! Im weiteren Tagesverlauf warten Podien zu Mobilität, der Welt der Beziehungen und sozialen Garantien auf uns alle. Abgerundet wird der Tag dann mit einer Diskussion zu **„Und wie kommen wir zur Utopie? – Wege der sozial-ökologischen Transformation“** mit Elisabeth Jeglitzka, Uwe Meinhardt, Ceren Türkmen, Nilufer Koç und Linda Schneider. Tipp für Menschen, die grade in Leipzig sind: von 15:00-18:00 läuft im "heiter bis wolkig" in Plagwitz wieder die **Utopiemaschine** heiß - kommt gerne vorbei!

Außerdem möchten wir euch folgende Einladung an alle Teilnehmer*innen nicht vorenthalten: "**Utopien verbinden** - läuft bei uns! **Seid auch mit dabei** zum strangübergreifenden Austausch und Verbinden. Nochmal am Freitag ab 17 Uhr im Raum "Utopien verbinden".

Nun noch der Hinweis, dass Workshop (241) Kreiskultur, für morgen Freitag, den 28.08. leider abgesagt wurde.

Wir wünschen eine gute Nacht, erfolgreiches Eindrückeverarbeiten und einen wunderbar utopischen Freitag.

P.S.: schon eine Tech-Host-Schicht übernommen?

---

*27.08., 18:15* **++ Sei kein Tech-Horst – werde Tech-Host! ++ Teilt eure Erlebnisse mit uns ++ Open Space-Angebote einreichen ++**

Soeben erfahren wir, dass es auch im digitalen Bereich Verspätungen geben kann. Aber was uns in unserem nicht-digitalen Alltag auch ständig vorkommt, kann uns beim digitalen Kongress auch nicht umhauen. Umso mehr freuen wir uns, dass inzwischen alle Nachmittagspodien gestartet sind.

Damit nicht nur die (technisch komlexen) Podien in guten Händen sind, sondern auch die (technisch weniger komplexen) Workshops gut laufen, suchen wir weiterhin **Tech-Hosts**. Diese Aufgabe erfordert eine kurze Einführung vom Orga-Team und die Bereitschaft, bei einem 90-minütigen Workshop dabei zu sein. Meldet euch sehr gerne per [**E-Mail**](mailto:zfa-digital@riseup.net) bei uns.

Ganz anderes Feld: **der Abschluss am Sonntag**. Dazu Folgendes: Wir suchen noch Menschen, die am Sonntag morgen Lust haben, ihre Kongresserfahrung kurz zu teilen: Was war für dich besonders? Welcher Moment hat dich inspiriert? Welche Verbindungen zwischen den einzelnen Themen und einer umfassenden Vision ist für dich deutlich geworden? Entweder "live" oder über ein eingespieltes Video. Melde dich gerne unter [**zfa-programm@riseup.net**](mailto:zfa-programm@riseup.net).

Und zum Abschluss: um 19:00 herum schließt die **Open Space** Redaktion für heute. Wenn ihr für morgen einen Open Space ankündigen möchtet, macht dies gerne [**per E-Mail**](mailto:zfa-openspace@riseup.net).

Nun wünschen wir euch einen inspirierenden und unterhaltsamen Abend!

---

*27.08., 09:15* **++ Tag 3 startet ++ Finanzierungslücke noch nicht geschlossen ++ Schon Tech-Host gewesen?++**

Guten Morgen allerseits!

Gleich werden Kate Čabanová (Konzeptwerk Neue Ökonomie) und Werner Rätz (attac) gemeinsam mit uns in den Tag starten. Wie immer [**live hier**](streams/stream1) anzusehen.

Der enorme Aufwand der letzten Monate, die Umstellung auf einen digitalen Kongress - wir haben schon nach den ersten eineinhalb Tagen den Eindruck: es hat sich gelohnt! Allerdings sitzen wir derzeit noch auf einer Finanzierungslücke von gut 40.000€. Dafür sind wir auf eure Unterstützung angewiesen. [**Für Spendeninfos lest hier**](https://zukunftfueralle.jetzt/ueber-uns/spenden).

Tech-Host sein - das ist das In-Thing dieses Kongresses. Wie das geht? Einfach. Was ihr mitbringen müsst? Ein bisschen Vorbereitungszeit und 90 Minuten Zeit für die Workshopbetreuung. Wir suchen dringend noch Tech-Hosts. Lest [**mehr dazu hier**](https://zukunftfueralle.jetzt/2020/08/24/dringend-tech-hosts-gesucht/).

{% include button.md text="Zur Spendenseite" url="https://zukunftfueralle.jetzt/ueber-uns/spenden" %}


---

*26.08., 22:00* **++ Tech-Hosts gesucht ++**

Wie ihr gerade zum Abschluss des wunderbaren Podiums hören konntet: auch ein digitaler Kongress lebt davon, dass Teilnehmer*innen mithelfen. Insbesondere bei den Tech-Host-Schichten suchen wir immer noch viele helfende Hände! Lest mehr dazu hier: [**Dringend: Tech-Hosts gesucht!**](https://zukunftfueralle.jetzt/2020/08/24/dringend-tech-hosts-gesucht/). ["Tech-Host" klingt komplizierter als es ist ;)]

Gute Nacht und bis morgen!

---

*26.08., 19:55* **++ Open Spaces sind online ++ Abendpodium geht gleich los! ++**

Ein erster vollständiger Zukunft Für Alle Kongress-Tag neigt sich dem Ende. Nicht alles lief ganz glatt, aber insgesamt haben sich heute viele hundert Menschen in Workshops ausgetauscht und Podiumsdiskussionen angesehen. Und wir gehen davon aus, dass insbesondere die Probleme beim Finden der Workshopräume abnehmen werden ;) Wir möchten euch nochmal auf die [**Bezugsgruppen**](bezugsgruppen), den [**Open Space**](open-spaces) und die [**Bar**](bar) hinweisen. All diese Formate sind für euch angelegt, zur Vernetzung außerhalb von Workshops. In der Bar haben alle Zutritt, auch nicht angemeldete Teilnehmer\*innen. Und zum Open Space: morgen finden dort die ersten beiden Veranstaltungen statt, schaut doch mal vorbei. Und vielleicht organisiert ihr ja einen Open Space für Freitag?!

Und nun wünschen wir euch viel Spaß beim Abendpodium "Abendpodium - Ökonomie der Zukunft - Wie organisieren wir demokratische und bedürfnisorientierte (Re-)Produktion?". Grüße gehen raus nach Freiburg, Schwerin, Sehlis und München, wo heute [**Livestreaming Veranstaltungen**](livestreaming-events) stattfinden!

Achso, und wenn ihr noch eine Abendlektüre sucht (und weil wir gestern den falschen Link geteilt haben):

{% include button.md text="Zukunft Für Alle Broschüre" url="https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/ZFA_Buch_digital-1.pdf" %}

---

*26.08., 14:20* **++ Verschiebung: "Waldweit unterwegs" auf 27.08. 08:45 verschoben ++**

Das Theater "Waldweit unterwegs" wird witterungsbedingt von heute Nachmittag auf morgen (27.08.) früh um 08:45 Uhr verschoben. Ihr seid alle herzlich eingeladen mitzukommen, ob angemeldet oder nicht, in den Morgenstunden den Leipziger Auwald performativ zu erkunden.

---

*26.08., 12:15* **++ Kurzfristige Workshopanmeldungen sind möglich – aber manchmal ein bisschen kompliziert ++**

Ihr habt euch für einen Workshop angemeldet[**hier**](https://anmeldung.zukunftfueralle.jetzt/), aber der Workshopraum wird im [**Kongressgelände**](https://kongressgelaende.zukunftfueralle.jetzt/) nicht angezeigt? Hier ein Weg, wie ihr den Raum doch noch finden könntet:
1. Gehe auf die Anmeldeseite: [**hier klicken**](https://anmeldung.zukunftfueralle.jetzt/)
2. Scrolle solange runter, bis ihr bei dem Workshop angekommen seid, für den ihr euch schon angemeldet habt. Gefunden? Dann:
3. Klicke auf der rechten Seite "cancel" bzw. "abmelden". Die Seite lädt nun neu.
4. Scrolle wieder zu deinem Workshop und klicke nun auf "register" bzw. "anmelden"
5. Es wird dir nun ein neuer Zugangslink angezeigt sowie per Mail zugesandt. Mit diesem Link sollte sollte der Raum auch auf dem Kongressgelände für dich sichtbar werden.

---

*26.08., 11:30* **++ Wo ist mein Workshop? Da! ++**

Ihr seid bereits auf dem [digitalen Kongressgelände](https://kongressgelaende.zukunftfueralle.jetzt/), aber seht dort nur die Übersichtskarte und findet euren Raum nicht? Keine Panik! **Links oben in der Ecke seht ihr drei weiße Striche: klickt dort drauf und es wird sich eine Übersicht über alle Räume öffnen, zu denen ihr Zugang habt.** Und jetzt: viel Spaß!

---

*26.08., 11:00* **++ Tag 2 geht los! ++**

Vielen Dank an Natasha A. Kelly für den inspirierenden Morgenauftakt!

Jetzt geht es mit den Workshops los. Falls ihr euch per Telefon in euren Wokshop einwählen möchtet, beachtet, dass sich einige Telefonnummern geändert haben. [**Schaut hier nach, bitte**](https://kongresskarte.zukunftfueralle.jetzt/de/dial-in). Wir wünschen einen guten digitalen Start ins Workshopprogramm.

Ansonsten heute unter anderem im Programm und per [**Livestream**](livestreams) sichtbar:

* Podium Wohnen
* Podium Landwirtschaft und Ernährung
* Podium Digitalität
* Podium Energie
* Abendpodium "Ökonomie der Zukunft"


---


*25.08., 18:00* **++ Zukunft Für Alle – Die Broschüre ++**

Es ist geschafft! Die Streams stehen, die Übersetzungen laufen an und funktionieren inzwischen durchgehend. Danke an alle, die das ermöglicht haben und ununterbrochen weiterhin ermöglichen: die vielen Helfer\*innen, von der Küche bis zur Regie ebenso wie die grandiosen Sprecher\*innen. Weil die Broschüre **"Zukunft Für Alle"** während des Auftaktes immer wieder genannt wurde stellen wir hier nochmal den Downloadlink rein, wünschen euch eine inspirierende Lektüre und zudem natürlich einen schönen Abend!

Achja: die Broschüre ist kostenfrei erhältich, aber nicht kostenlos entstanden. Unterstützt doch gerne das [**Crowdfunding-Projekt**](https://www.oekom-crowd.de/projekte/zukunft-fuer-alle/) zur Broschüre!

{% include button.md text="Zukunft Für Alle Broschüre" url="https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/ZFA_Buch_digital-1.pdf" %}


---

*25.08., 15:30* **++ Der Kongress geht los! ++**

Let's go! Für alle, die gleich den Kongressauftakt mit Bini Adamczak, Nina Treu und Kai Kuhnhenn streamen möchten: schaut in unseren [**Livestreamsaal Futur I**](streams/stream1). Wenn ihr die Anmeldung für Workshops verpasst haben solltet: kein Problem! Bisher sind gut 2/3 aller Plätze in Anspruch genommen. Ihr könnt euch weiterhin [**anmelden**](https://contacts.knoe.org/content/anmeldung-zukunft-fuer-alle), jedoch noch nicht für konkrete Workshops. Ab heute Abend könnt ihr euch dann [**hier**](https://anmeldung.zukunftfueralle.jetzt/) eure Workshops auswählen. Wir freuen uns auf einen inspirierenden Kongress und sechs Tage, an denen wir uns mit voller Aufmerksamkeit den Fragen widmen: Wie wollen wir 2048 leben? Und wie kommen wir dahin?

Auf eine gute Zeit!

Achja: heute Abend schon was vor? In eurem [**Lieblingslivestream**](streams/stream1) wird das Podium "Aus Krisen von heute die Zukunft entwickeln“ mit Antje von Broock, Tonny Nowshin und Tina Röthig, moderiert von Ruth Krohn übertragen.

---

*24.08., abends* **++ Zukunftszentrale und Kongressinfrastruktur werden eingerichtet ++**<br>

Ein ereignisreicher Tag neigte sich dem Ende zu, als wir gerade die Türen der Zukunftszentrale hinter uns schlossen. Der Vorbereitungskreis sah sich erstmals seit Februar in größerer Runde wieder. Doch Zeit für Pläuschchen war nur wenig. Es ist viel passiert, doch wir fassen es kurz: wir alle freuen uns riesig auf morgen und sind wahnsinnig gespannt auf den Zukunft Für Alle Kongress! Noch einmal schlafen, dann geht's los: 10:00 Pressekonferenz, 16:00 Auftaktplenum, 16:30 spricht Bini Adamczak zu "Wie Utopie?" und um 20:00 geht dann das Eröffnungspodium: "Aus Krisen von heute die Zukunft entwickeln". [Alle Veranstaltungen werden im Kanal Futur I gestreamt.](streams/stream1)

---

*20.08., 10:30* **++ Die Karte des digitalen Kongressgeländes ist eingebettet! ++**<br>

Freund\*innen und Kämpfer\*innen der gerechten und ökologischen Zukunft, ein großer Schritt Richtung digitalem Kongress ist getan worden. Die Karte ist seit sieben Stunden online. Eingeschlafen – keine Karte. Aufgewacht – Karte! Jetzt müssen nur noch die Links darauf aktiviert, die Unterseiten fertig befüllt und alles öffentlich zugänglich gemacht werden. Das schaffen wir auch noch, keine Sorge.

Bis baldrian!

---
