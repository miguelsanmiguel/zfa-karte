---
layout: space
title:  "Tech-Support"
in_sorted_list: true
svg: support
---
**Erstens**: Wenn ihr euren per E-Mail versendeten Anmeldelink noch nicht geklickt habt: erledigt das bitte bevor ihr auf dieser Website auf Links zu den diversen Kongressräumen oder den "Eingang" oben rechts auf dieser Seite klickt. Nur mit diesem Link erhaltet ihr Zugang zur Kongresssoftware venueless, in der Workshops, Open Spaces, Bezugsgruppentreffen u.v.m. stattfinden. Wenn du in deinem Browser den "local storage" aktiviert hast, wird dein Zugang ("Token") gespeichert und du kannst alle auf der Kongresskarte angezeigten Linkbuttons, die in verschiedene venueless-Räume führen nutzen und gelangst direkt in den verlinkten Raum. Alternativ kannst du deine Privatsphäre schützen, den local storage weiterhin verschlossen lassen und dich stets über deinen persönlichen Zugangslink wieder einwählen. Die Buttons auf dieser Seite hier funktionieren dann für dich nicht.

**Zweitens:** Ihr seid bereits auf dem [digitalen Kongressgelände](https://kongressgelaende.zukunftfueralle.jetzt/), aber seht dort nur die Übersichtskarte und findet euren Raum nicht? Keine Panik! **Links oben in der Ecke seht ihr drei weiße Striche: klickt dort drauf und es wird sich eine Übersicht über alle Räume öffnen**, zu denen ihr Zugang habt. 

Und nun:
**Hallo, herzlich willkommen!**

Wir sind da, um möglicherweise auftretende technische Probleme während deines Kongressaufenthaltes schnellst möglich zu beheben. Da jede*r von uns immer nur einer Person auf einmal weiterhelfen kann, sind unsere Ressourcen begrenzt. Deshalb haben wir auf dieser Seite verschiedene Anleitungen hochgeladen, die für eine technische Verbesserung deines Kongresserlebnisses hilfreich sein könnten:

- [**Kongress-ABC**](https://zukunftfueralle.jetzt/kongress/kongress-abc/)
- [**Leitfaden Teilnehmer*innen**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/LeitfadenTN.pdf)
- [**Anleitung Big Blue Button**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-Button-Tipps-angenehme-Videokonferenzen.pdf)
- [**Anleitung Tech-Hosts**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung.pdf)
- [**Online-Handbuch für Referierende**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Online-Handbuch-für-Referierende_2.0.pdf)

## Erreichbarkeit
Wenn das nicht weiterhilft, **benutze bitte bevorzugt einen der Techsupport-Chatkanäle auf "venueless"** oder ruf uns an unter +4934165673750. Wir bitten um  Verständnis, dass wir aus Kapazitätsgründen individuelle Probleme nur für angemeldete Teilnehmer*innen bearbeiten können.

