---
layout: space
title:  "Telefonische Einwahldaten Workshopräume"
permalink: dial-in
svg: support
---
Hier sind alle Telefonnummern zur Einwahl in die Workshops. Beachtet bitte, dass sich einige Nummern geändert haben. Die Nummer für euren Workshop findet ihr auch zu jeder Zeit im Chat des Workshopraums.

- 0341 98 97 86 10 bleibt gleich
- 0341 98 97 86 11 bleibt gleich
- 0341 98 97 86 12 bleibt gleich
- 0341 98 97 86 13 bleibt gleich
- 0341 98 97 86 14 bleibt gleich
- 0341 98 97 86 15 bleibt gleich
- 0341 98 97 86 16 bleibt gleich
- 0341 98 97 86 17 bleibt gleich
- 0341 98 97 86 18 bleibt gleich
- 0341 98 97 86 19 bleibt gleich
- 0341 98 97 86 20 wurde geändert zu **0341 98 97 86 60**
- 0341 98 97 86 21 wurde geändert zu **0341 98 97 86 61**
- 0341 98 97 86 22 wurde geändert zu **0341 98 97 86 62**
- 0341 98 97 86 23 wurde geändert zu **0341 98 97 86 63**
- 0341 98 97 86 24 wurde geändert zu **0341 98 97 86 64**
- 0341 98 97 86 25 wurde geändert zu **0341 98 97 86 65**
- 0341 98 97 86 26 wurde geändert zu **0341 98 97 86 66**
- 0341 98 97 86 27 wurde geändert zu **0341 98 97 86 67**
- 0341 98 97 86 28 wurde geändert zu **0341 98 97 86 68**
- 0341 98 97 86 29 wurde geändert zu **0341 98 97 86 69**
- 0341 98 97 86 30 wurde geändert zu **0341 98 97 86 90**
- 0341 98 97 86 31 wurde geändert zu **0341 98 97 86 91**
- 0341 98 97 86 32 wurde geändert zu **0341 98 97 86 92**
- 0341 98 97 86 33 wurde geändert zu **0341 98 97 86 93**
- 0341 98 97 86 34 wurde geändert zu **0341 98 97 86 94**