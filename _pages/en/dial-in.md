---
layout: space
title:  "Phone numbers for workshop rooms"
permalink: dial-in
svg: support
---
Here are all telephone numbers for the workshop sessions. Beware that a few numbers have changed. You will also find your workshop's phone number at any time in the respective workshop's chat.

- 0341 98 97 86 10 remains the same
- 0341 98 97 86 11 remains the same
- 0341 98 97 86 12 remains the same
- 0341 98 97 86 13 remains the same
- 0341 98 97 86 14 remains the same
- 0341 98 97 86 15 remains the same
- 0341 98 97 86 16 remains the same
- 0341 98 97 86 17 remains the same
- 0341 98 97 86 18 remains the same
- 0341 98 97 86 19 remains the same
- 0341 98 97 86 20 changes to **0341 98 97 86 60**
- 0341 98 97 86 21 changes to **0341 98 97 86 61**
- 0341 98 97 86 22 changes to **0341 98 97 86 62**
- 0341 98 97 86 23 changes to **0341 98 97 86 63**
- 0341 98 97 86 24 changes to **0341 98 97 86 64**
- 0341 98 97 86 25 changes to **0341 98 97 86 65**
- 0341 98 97 86 26 changes to **0341 98 97 86 66**
- 0341 98 97 86 27 changes to **0341 98 97 86 67**
- 0341 98 97 86 28 changes to **0341 98 97 86 68**
- 0341 98 97 86 29 changes to **0341 98 97 86 69**
- 0341 98 97 86 30 changes to **0341 98 97 86 90**
- 0341 98 97 86 31 changes to **0341 98 97 86 91**
- 0341 98 97 86 32 changes to **0341 98 97 86 92**
- 0341 98 97 86 33 changes to **0341 98 97 86 93**
- 0341 98 97 86 34 changes to **0341 98 97 86 94**